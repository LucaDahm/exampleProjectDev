package unitTest

import (
	"backend/backend/main/api"
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"net/http"
	"net/http/httptest"
	"testing"
)

var expection string = "Hello World!"

func TestDatabaseQuery(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {

	}
	defer db.Close()

	mock.ExpectQuery(`SELECT \* FROM example`).WithArgs().WillReturnRows(sqlmock.NewRows([]string{"handshake"}).AddRow(expection))

	result, err := api.DatabaseInterface.Query(db, "SELECT * FROM example")
	if err != nil {
		t.Errorf("Error should be nil")
	}
	if result != expection {
		t.Errorf("handshake should match")
	}
}

func TestDatabaseQueryEmpty(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {

	}
	defer db.Close()

	mock.ExpectQuery(`SELECT \* FROM example`).WithArgs().WillReturnRows(sqlmock.NewRows([]string{}))

	result, err := api.DatabaseInterface.Query(db, "SELECT * FROM example")
	if result == " " {
		t.Errorf("Result should be empty")
	}

}

func TestServiceImplGetHandshakeService(t *testing.T) {
	databaseMock := DatabaseInterfaceMock{}
	databaseStore := api.DatabaseInterface
	databaseMock.response = func(s string) (string, error) {
		if s == "SELECT * FROM example" {
			return expection, nil
		} else {
			return "", errors.New("wrong statement")
		}
	}
	api.DatabaseInterface = databaseMock
	result, err := api.HandshakeService.GetHandshakeService()

	if err != nil {
		t.Errorf("Query should match")
	}
	if result != expection {
		t.Errorf("handshake should match")
	}

	api.DatabaseInterface = databaseStore
}

type DatabaseInterfaceMock struct {
	response func(string) (string, error)
}

func (d DatabaseInterfaceMock) OpenDatabase() (*sql.DB, error) {
	return nil, nil
}

func (d DatabaseInterfaceMock) Query(db *sql.DB, statement string) (string, error) {
	return d.response(statement)
}

func TestGetHandshake(t *testing.T) {
	serviceMock := ServiceInterfaceMock{}
	serviceStore := api.HandshakeService
	serviceMock.response = func() (string, error) {
		return expection, nil
	}
	api.HandshakeService = serviceMock

	request, err := http.NewRequest("GET", "/", nil)
	response := httptest.NewRecorder()
	api.Router().ServeHTTP(response, request)

	var result string
	err = json.NewDecoder(response.Body).Decode(&result)

	if err != nil {
		t.Errorf("Query should match")
	}
	if result != expection {
		t.Errorf("handshake should match")
	}

	api.HandshakeService = serviceStore
}

type ServiceInterfaceMock struct {
	response func() (string, error)
}

func (s ServiceInterfaceMock) GetHandshakeService() (string, error) {
	return s.response()
}
