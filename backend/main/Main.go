package main

import (
	"backend/backend/main/api"
	"log"
	"net/http"
)

func main() {
	router := api.Router()
	log.Fatal(http.ListenAndServe(":4200", router))
}
