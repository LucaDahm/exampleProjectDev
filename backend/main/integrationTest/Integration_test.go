package integrationTest

import (
	api2 "backend/backend/main/api"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

var expec string = "Hello World!"

func TestOpenDatabase(t *testing.T) {
	db, err := api2.DatabaseInterface.OpenDatabase()
	err = db.Ping()
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestGetRequest(t *testing.T) {

	request, err := http.NewRequest("GET", "/", nil)
	response := httptest.NewRecorder()
	api2.Router().ServeHTTP(response, request)

	var result string
	err = json.NewDecoder(response.Body).Decode(&result)

	if err != nil {
		t.Errorf("Query should match")
	}
	if result != expec {
		t.Errorf("handshake should match")
	}
}
