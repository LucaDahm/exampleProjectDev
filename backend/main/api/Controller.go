package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", GetHandshake).Methods("GET")
	return router
}

func GetHandshake(writer http.ResponseWriter, request *http.Request) {
	out, _ := HandshakeService.GetHandshakeService()
	json.NewEncoder(writer).Encode(out)
}
