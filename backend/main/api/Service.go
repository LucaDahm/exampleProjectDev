package api

type ServiceImpl struct{}

var (
	HandshakeService Service = ServiceImpl{}
)

func (s ServiceImpl) GetHandshakeService() (string, error) {
	db, err := DatabaseInterface.OpenDatabase()

	out, err := DatabaseInterface.Query(db, "SELECT * FROM example")
	return out, err
}
