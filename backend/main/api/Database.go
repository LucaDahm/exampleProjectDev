package api

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

const sqlDb string = ":@tcp(mysql:3306)/test"

type DatabaseImpl struct{}

var (
	DatabaseInterface Database = DatabaseImpl{}
)

func (d DatabaseImpl) OpenDatabase() (*sql.DB, error) {
	db, err := sql.Open("mysql", sqlDb)
	return db, err
}

func (d DatabaseImpl) Query(db *sql.DB, statement string) (string, error) {
	defer db.Close()
	result, err := db.Query(statement)
	db.Close()
	var handshake string
	for result.Next() {
		_ = result.Scan(&handshake)
	}
	return handshake, err
}
