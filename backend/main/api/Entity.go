package api

import "database/sql"

type Service interface {
	GetHandshakeService() (string, error)
}

type Database interface {
	OpenDatabase() (*sql.DB, error)
	Query(db *sql.DB, statement string) (string, error)
}
